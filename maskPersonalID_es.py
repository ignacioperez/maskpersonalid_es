#!/usr/bin/python
# -*- coding: utf-8 -*-

def mask_personal_id(personal_id, mask_char='*'):
    """
        More info: https://gitlab.com/ignacioperez/maskpersonalid_es
    """
    import re
    digits = filter(lambda x: x.isdigit(), personal_id)
    # DNI: reveal 4th, 5th, 6th and 7th digits
    if re.match(r'^\d{8}\w$', personal_id):
        return mask_char*3 + digits[3:7] + mask_char*2
    # NIE: reveal 4th, 5th, 6th and 7th digits
    elif re.match(r'^\w\d{7}\w$', personal_id):
        return mask_char*4 + digits[3:7] + mask_char
    # Passport: reveal 3th, 4th, 5th and 6th digits
    elif re.match(r'^\w{3}\d{6}$', personal_id):
        return mask_char*5 + digits[2:6]
    # Digits > 7: reveal 4th, 5th, 6th and 7th digits
    elif len(digits) > 7:
        char_n = 0
        masked_id = ''
        for char in personal_id:
            if char.isdigit():
                char_n += 1
            if char.isdigit() and char_n in [4, 5, 6, 7]:
                masked_id += char
            else:
                masked_id += mask_char
        return masked_id
    # Other: only the last four characters
    return mask_char * (len(personal_id) - 4) + personal_id[-4:]