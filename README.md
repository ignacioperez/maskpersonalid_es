# Description

Enmascaramiento de protección del DNI, NIE, pasaporte o equivalente según las recomendaciones de la AEPD (Agencia Española de Protección de Datos) en aplicación de la LOPDGDD 3/2018.

# Usage:
```python
> from maskPersonalID_es import mask_personal_id

> # DNI: reveal 4th, 5th, 6th and 7th digits
> mask_personal_id('12345678X')
'***4567**'

> # DNI with -
> mask_personal_id('12345678X', '-')
'---4567--'

> # NIE: reveal 4th, 5th, 6th and 7th digits
> mask_personal_id('L1234567X')
'****4567*'

> # Passport: reveal 3th, 4th, 5th and 6th digits
> mask_personal_id('ABC123456')
'*****3456'

> # Digits > 7: reveal 4th, 5th, 6th and 7th digits
> mask_personal_id('XY12345678AB')
'*****4567***'

> # Another example with digits > 7
> mask_personal_id('X123Y45A6B78')
'*****45*6*7*'

> # Other: only the last four characters
> mask_personal_id('ABCD123XY')
'*****23XY'
```